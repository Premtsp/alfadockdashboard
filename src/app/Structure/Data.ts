export class Data{
    dwgCompletedCount: any;
    dwgFailedCount: any;
    office5SCount: any;
    office5SFailedCount: any;
    officeFileCount: any
    socketFileCount: any;
    dwgCompleted: any;
    phdStatusCompleted: any;
    phdStatusPlaced: any;
    phdStatusProcessing: any;
    thumbnailCount: any;
    thumbnailFailed: any;
    officeFailedCount: any;
    officeCompletedCount: any;
    loginName: any[];
    files: {};
}