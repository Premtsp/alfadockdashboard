import { Component, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { Chart } from 'chart.js'
import { DashboardDataService } from './dashboard-data.service'
import { Data } from './Structure/Data'
import { Key } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private dashboardService: DashboardDataService){
    
  }
  ngOnInit(){
    const source = interval(10000);
    this.dwgToggle = true;
    this.subscription = source.subscribe(val => {
      this.toggleDwg()
      this.togglePhd() 
    });
    
    this.dashboardService.getData().subscribe({next:(data:Data) =>{
        console.log(data)
        this.dwgCompletedCount = data.dwgCompletedCount
        this.dwgFailedCount = data.dwgFailedCount;
        this.officeFileCount = data.officeFileCount
        this.socketFileCount = data.socketFileCount
        if(this.dwgFailedCount + this.dwgCompletedCount)
          this.dwgSuccess = Number((this.dwgCompletedCount/(this.dwgFailedCount + this.dwgCompletedCount))*100).toFixed(2)
        else this.dwgSuccess = "0";
        this.phdStatusCompleted = data.phdStatusCompleted
        this.phdStatusProcessing = data.phdStatusProcessing
        this.phdStatusPlaced = data.phdStatusPlaced
        this.thumbnailCount = data.thumbnailCount
        this.thumbnailFailed = data.thumbnailFailed
        if(this.thumbnailCount + this.thumbnailFailed > 0)
          this.thumbnailSuccess = Number((this.thumbnailCount/(this.thumbnailCount + this.thumbnailFailed))*100).toFixed(2)
        else this.thumbnailSuccess = "0";
        this.office5SCount = data.office5SCount;
        this.office5SFailedCount = data.office5SFailedCount;
        if(this.office5SCount + this.office5SFailedCount > 0)
          this.office5SSuccess = Number((this.office5SCount/(this.office5SCount + this.office5SFailedCount))*100).toFixed(2)
        else this.office5SSuccess = "0"
        document.getElementById('dwgBar').style.width = this.dwgSuccess+"%"
        document.getElementById('thumbnailBar').style.width = this.thumbnailSuccess+"%"
        document.getElementById('officeBar').style.width = this.office5SSuccess+"%"
        this.files = data.files;
       
        console.log(this.files)
        this.labelsChart = Object.keys(this.files)
        //this.labelsChart = ['Mori','Yakanova','Hueng-Min Son','Alfa-Dock','Ai-Process navi','Johnny Bravo']
        //this.dataChart = [50123, 12453, 38, 159, 5553, 128643]
        this.dataChart = Object.values(this.files)
        this.loginName = data.loginName

        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: this.labelsChart,
            datasets: [{
                label: '# of Votes',
                data: this.dataChart,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    },
  error: () => {
    console.log("Error")
  }}
    )
  
  }
  myDate = new Date();
  phdStatus: number = 1;
  folderDetails = [10,20,30,40]
  title = 'DashboardApp';
  subscription: Subscription;
  dwgToggle:boolean;
  socketFileCount: number;
  dwgFailedCount:number;
  dwgCompletedCount:any;
  dwgSuccess:string;
  office5SCount: number;
  office5SFailedCount: number;
  office5SSuccess: string;
  officeFileCount:number;
  phdStatusCompleted: number;
  phdStatusPlaced: number;
  phdStatusProcessing: number;
  thumbnailCount: number;
  thumbnailFailed: number;
  thumbnailSuccess: string;

  labelsChart: any[];
  dataChart: any[];
  loginName: any[];
  files: {}

  toggleDwg()
  {
    this.dwgToggle = !this.dwgToggle;
  }

  togglePhd()
  {
    this.phdStatus++;
    if(this.phdStatus === 4)
      this.phdStatus = 1;
  }

}
