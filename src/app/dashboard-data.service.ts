import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DashboardDataService {

myDate = new Date();
fromDate:string;
toDate: string;
constructor(private http:HttpClient){
  //this.fromDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
}

  getData(){
    const url = "../api/dashboard/getData";
    var params = new URLSearchParams();
    var temp = "2020-02-12";   
    const options = {
      headers: new HttpHeaders().append('Content-Type','application/x-www-form-urlencoded'),
      params: new HttpParams().append('fromdate', temp).append('todate', temp)
    }
    
    return (this.http.post(url, {},options))
  }
}
